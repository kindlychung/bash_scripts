#!/usr/bin/env bash

for fileIter in "$@"; do
    filesize=$(wc -c "$fileIter" | cut -f 1 -d ' ')
    echo $filesize
    if [[ $filesize -lt 10000  ]]; then
        rm "$fileIter"
    fi
done
