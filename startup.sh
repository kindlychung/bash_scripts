#!/bin/bash

# source ~/bin/bashutils/system.sh
# reporter=$(getreporter)

# keyboard remapping for x
# setxkbmap -option caps:swapescape
setxkbmap -option terminate:ctrl_alt_bksp
setxkbmap -option compose:ralt

# set LCD backlight
xbacklight -set 50

# make the cursor disappear when idle
# if command -v unclutter >/dev/null 2>&1; then
#     unclutter -display :0.0 -idle 1 &
# fi

# turn off touchpad
if command -v synclient >/dev/null 2>&1; then
    synclient TouchpadOff=1
fi

# don't start fcitx
# if command -v fcitx >/dev/null 2>&1; then
#     killall fcitx
# fi


gmrun &
xautolock -time 20 -locker '/home/kaiyin/bin/xflock4' &
