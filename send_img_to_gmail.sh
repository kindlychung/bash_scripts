#!/usr/bin/env bash

echo $$
f=$(tempfile)
echo $'\n\n==============\n\n' > "$f"
xsel -b >> "$f"
deepin-scrot -o /tmp/scrot_tmpout.png

# gvim "$f"
# # check modification time, make sure file has been saved
# fmtime1=$(stat -c %Y "$f")
# while (( $(stat -c %Y "$f") <= $fmtime1 ))
# do
#     sleep 1
#     continue
# done

# a better approach using kill -0 to test the process
vim "$f"
read -r firstline<"$f"
echo "start sending!"
if [[ $(echo $firstline | tr -d ' ') ]]
then
    mutt emma tumblr me pb bg -s "$firstline   sharetogmail" -a /tmp/scrot_tmpout.png  < "$f"
fi
