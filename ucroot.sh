#!/bin/bash
shopt -s dotglob
shopt -s globstar
shopt -s extglob



link_dir='/home/kaiyin/Dropbox/rootconfig'
for cloudf in $link_dir/**/*
do
    if [[ -f "$cloudf" ]]
    then
        local_relativepath="${cloudf#$link_dir/}"
        local_path="/$local_relativepath"

        # is it a symlink?
        if [[ -h "$local_path" ]]
        then
            echo "Linking $local_path to $cloudf"
            sudo ln -sf "$cloudf" "$local_path"
        elif [[ -f "$local_path" ]]
        then
            echo "Backing up $local_path"
            sudo mv "$local_path" "${local_path}.bk"
            echo "Linking $local_path to $cloudf"
            sudo ln -s "$cloudf" "$local_path"
        fi
    fi
done
