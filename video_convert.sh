#!/bin/bash

bitrate="250"
width="320"
height="240"
arate="128"

for file in "$@"; do
    echo "=> Transcoding '$file'... "

    dst=`dirname "$file"`
    new=`basename "$file" | sed 's@\.[A-Za-z][A-Za-z][A-Za-z]$@@'`.mp4
    echo $dst
    echo $new
    mencoder "$file" -idx -forceidx \
        -ovc lavc \
        -lavcopts vcodec=mpeg4:vme=5:vbitrate=$bitrate:vmax_b_frames=0 \
        -vf-add scale=$width:$height,expand=::-1:-1:1 \
        -ofps 25 -of lavf -lavfopts format=mp4 -oac mp3lame \
        -lameopts br=128:cbr:mode=1 -srate 44100 \
        -o "$new" \
        -vf-add harddup

    ls -lh "$file" "$dst/$new"
    echo
done
