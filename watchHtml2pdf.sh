#!/usr/bin/env bash

file=$(realpath "$1")
resfile="${file%.*}.pdf"
cmd="cutycapt --url=file://$file --out=$resfile --delay=1000 --javascript=on --plugins=on --auto-load-images=on --js-can-open-windows=on --js-can-access-clipboard=on"


if $cmd; then
	okular "$resfile" &
else
	exit 1
fi

# wid=$(xdotool search "$resfile" | head -1)

while inotifywait -e modify "$file"; do
    if ! $cmd; then
        kdialog --passivepopup "<h1>$cmd compile error!" 1
	# else
	# 	xdotool windowactivate $wid
	# 	xdotool key F5
    fi
done
