#!/usr/bin/env bash

pdffile=$1
cp --backup=numbered $pdffile ~/Desktop/
pdftk  $pdffile output x1.tmp uncompress
sed -e 's/Qoppa.*$//gI' x1.tmp > x2.tmp
sed -e 's/PDF Studio.*$//gI' x2.tmp > x1.tmp
pdftk x1.tmp output $pdffile compress
/bin/rm x*.tmp
