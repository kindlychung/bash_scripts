#!/usr/bin/env bash

rsync -av ~/personal_config_bin_files/workspace/collr \
    --delete \
    --exclude "*.so" --exclude "*.o" \
    tron:~/personal_config_bin_files/workspace
rsync -av ~/personal_config_bin_files/workspace/txtutils \
    --delete \
    --exclude "*.so" --exclude "*.o" \
    tron:~/personal_config_bin_files/workspace
rsync -av ~/personal_config_bin_files/workspace/rbed \
    --delete \
    --exclude "*.so" --exclude "*.o" \
    tron:~/personal_config_bin_files/workspace
rsync -av ~/personal_config_bin_files/workspace/manqq \
    --delete \
    --exclude "*.so" --exclude "*.o" \
    tron:~/personal_config_bin_files/workspace

# install them
# ssh tron '~/bin/installRpacks.sh'
