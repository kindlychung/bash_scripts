#!/bin/bash

stamptime=$(stat -c %Y $HOME/.dropbox_backup/stamp)
currenttime=$(date +%s)
timediff=$(expr $currenttime - $stamptime)
# echo $stamptime
# echo $currenttime
# echo $timediff

if [[ $timediff -ge 86400 ]]
then
    days=$(expr $currenttime / 86400)
    daysmod=$(expr $days % 7)
    weekday=$(echo "($daysmod + 3) % 7" | bc)
    targetdir="$HOME/.dropbox_backup/d$weekday"
    # echo $days
    # echo $daysmod
    # echo $targetdir
    # echo $weekday
    rsync -av "$HOME/Dropbox" "$targetdir"
    touch "$HOME/.dropbox_backup/stamp"
fi
