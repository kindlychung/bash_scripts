#!/usr/bin/env bash

for dirIter in ./*/; do
    cd "$dirIter"
    if [[ -e .git ]]; then
        echo -e '\n\n================================'
        echo Updating "$dirIter" git repo...
        echo '================================'
        gitpush.py a
    fi
    cd ..
done
