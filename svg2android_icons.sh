#!/usr/bin/env bash


if [[ "$OSTYPE" == cygwin  ]]; then
    INKSCAPE="/C/Program Files (x86)/Inkscape/inkscape.exe"
    OPTS='--export-background-opacity=0'
elif [[ "$OSTYPE" == linux-gnu  ]]; then
    INKSCAPE="/usr/bin/inkscape"
    OPTS='--without-gui --export-background-opacity=0'
elif [[ "$OSTYPE" == darwin*  ]]; then
    INKSCAPE="/Applications/Inkscape.app/Contents/Resources/bin/inkscape"
    OPTS='--without-gui --export-background-opacity=0'
else
    echo "Unknown OS!"
    exit 1
fi

# OSX
# INKSCAPE="/Applications/Inkscape.app/Contents/Resources/bin/inkscape"
# OPTS=--without-gui --export-background-opacity=0

SVG="$1"
DEST=../res
PNG=${SVG%.svg}.png


# PhoneGap Android
"$INKSCAPE" -w48 $OPTS --export-png=$DEST/drawable-mdpi/$PNG $SVG
"$INKSCAPE" -w72 $OPTS --export-png=$DEST/drawable-hdpi/$PNG $SVG
"$INKSCAPE" -w96 $OPTS --export-png=$DEST/drawable-xhdpi/$PNG $SVG
"$INKSCAPE" -w144 $OPTS --export-png=$DEST/drawable-xxhdpi/$PNG $SVG

# # PhoneGap iOS
# "$INKSCAPE" -w57 $OPTS --export-png=$DEST/ios/icon-57.png $SVG
# "$INKSCAPE" -w72 $OPTS --export-png=$DEST/ios/icon-72.png $SVG
# "$INKSCAPE" -w114 $OPTS --export-png=$DEST/ios/icon-57-2x.png $SVG
# "$INKSCAPE" -w144 $OPTS --export-png=$DEST/ios/icon-72-2x.png $SVG
