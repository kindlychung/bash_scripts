#!/bin/bash

for f in "${@}"; do
    ext=${f##*.}
    filename=${f%.*}
    cp "$f" "$filename.BK.$ext"
done
