#!/bin/bash

# rsync -av \
# --exclude "beep.mp3" \
# --exclude "beep.wav" \
# --exclude "bin" \
# --exclude "config" \
# --exclude "docs" \
# --exclude "etc" \
# --exclude "mylib" \
# --exclude "opt_small" \
# --exclude "R_private" \
# --exclude "rsync_ignore.txt" \
# --exclude "small_scripts" \
# --exclude "test" \
# --exclude "wordpress.com" \
# --exclude ".dropbox" \
# --exclude ".dropbox.cache" \
# /home/kaiyin/Dropbox/ ~/

# for clouddir in ~/Dropbox/link_separately/*
# do
#     for cloudfile in $clouddir/*
#     do
#         parent_basen=$(basename $clouddir)
#         child_basen=$(basename $cloudfile)
#         localfile="/home/kaiyin/$parent_basen/$child_basen"
#         echo "Removing $localfile"
#         rm -r $localfile
#         echo "Linking $cloudfile to $localfile"
#         ln -s $cloudfile $localfile
#     done
# done

shopt -s dotglob
shopt -s globstar
shopt -s extglob


for cloudfile in $HOME/Dropbox/*
do
    [[ $(basename "$cloudfile") = .dropbox ]] && continue
    [[ $(basename "$cloudfile") = .dropbox.cache ]] && continue
    child_basen=$(basename "$cloudfile")
    localfile="$HOME/$child_basen"
    echo "Removing $localfile"
    rm -rf "$localfile"
    echo "Linking $localfile to $cloudfile"
    ln -s "$cloudfile" "$localfile"
done

link_dir='$HOME/Dropbox/link_separately'
for cloudf in $link_dir/**/*
do
    if [ -f "$cloudf" ]
    then
        local_relativepath="${cloudf#$link_dir/}"
        local_path="$HOME/$local_relativepath"
        echo "Removing $local_path"
        rm -f "$local_path"
        echo "Linking $local_path to $cloudf"
        ln -s "$cloudf" "$local_path"
    fi
done
