#!/usr/bin/env bash

myip=$(ifconfig eth0 | grep 'inet addr' | perl -pe 's/.*inet addr:(\d+\.\d+\.\d+\.\d+).*/$1/')
# mywifiip=$(ifconfig eth1 | grep 'inet addr' | perl -pe 's/.*inet addr:(\d+\.\d+\.\d+\.\d+).*/$1/')
mywifiip=$(ifconfig wlan0 | grep 'inet addr' | perl -pe 's/.*inet addr:(\d+\.\d+\.\d+\.\d+).*/$1/')

exec kdialog --msgbox "\
	<font face='monospace' color=black> \
    <p>___Date: $(date +%D)</p> \
    <p>___Time: $(date +%R)</p> \
    <p>wire IP: $myip</p> \
	<p>wifi IP: $mywifiip</p> \
	</font>" \
    --title "Date, Time and Info"
