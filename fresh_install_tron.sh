#!/usr/bin/env bash

######################################################################
####  settings
######################################################################
# alt + right click, for resizing windows
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button true
gsettings set org.gnome.desktop.wm.preferences focus-new-windows strict
gsettings set org.gnome.desktop.wm.preferences focus-mode click
gsettings set org.gnome.desktop.wm.preferences auto-raise true
gsettings set org.gnome.settings-daemon.peripherals.touchpad scroll-method two-finger-scrolling
gsettings set org.gnome.settings-daemon.peripherals.touchpad natural-scroll true
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:swapescape']"
gsettings set org.gnome.desktop.interface text-scaling-factor 1.2


# app shortcuts
## add empty keybinding entries
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings \
    "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/']"

## browser
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "browser"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh chromium-browser"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Primary><Shift><Alt>b"
## file manager
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "fileManager"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh dolphin"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Primary><Shift><Alt>f"
## terminal
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ name "terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh gnome-terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ binding "<Primary><Shift><Alt>t"
## gmrun
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ name "runDialog"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh gmrun"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ binding "<Primary><Shift><Alt>g"

######################################################################
####  end settings
######################################################################

# ppa
sudo add-apt-repository ppa:fcitx-team/nightly &&\
sudo add-apt-repository ppa:webupd8team/java &&\
sudo add-apt-repository ppa:hydr0g3n/qbittorrent-stable &&\
sudo add-apt-repository ppa:zedtux/naturalscrolling && \
sudo apt-get update &&\
sudo apt-get upgrade &&\
sudo apt-get install \
    inkscape gimp gmrun chromium-browser thunar qbittorrent \
    virtualbox aptitude wine xsel \
rar unrar p7zip-full xtrlock \
pyside-tools gnome-screensaver \
git nvidia-331 python-appindicator \
 python3-pyside python3-pip dolphin\
    fcitx fcitx-table-wubi fcitx-pinyin xdotool\
    oracle-java7-installer r-base dconf-editor vim-gtk &&\

sudo pip3 install watchdog &&\
echo 'pointer = 1 2 3 5 4 7 6 8 9 10 11 12' > ~/.Xmodmap && xmodmap ~/.Xmodmap
