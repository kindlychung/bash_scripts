#!/usr/bin/env bash

cmd="latex -interaction=nonstopmode -halt-on-error"
file=$1
resfile="${1%.*}.dvi"

if $cmd "$file"; then
	okular "$resfile" &
else
	exit 1
fi

wid=$(xdotool search "$resfile" | head -1)

while inotifywait -e modify "$file"; do
    if ! $cmd "$file"; then
        kdialog --passivepopup "<h1>$cmd compile error!" 1
    fi
    if ! $cmd "$file"; then
        kdialog --passivepopup "<h1>$cmd compile error!" 1
	else
		xdotool windowactivate $wid
		xdotool key F5
    fi
done
