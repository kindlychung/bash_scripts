#!/usr/bin/env bash

url=$(pbpaste)
if [[ $url != http* ]]; then
    terminal-notifier -title get_clipboard_img -message "No url in clipboard!"
    exit 1
fi
wget -O /tmp/get_clipboard_image_sh_tmp.png "$url"
terminal-notifier -title get_clipboard_img  -message "Image downloaded."
imgcpmac /tmp/get_clipboard_image_sh_tmp.png
terminal-notifier -title get_clipboard_img -message "Image copied to clipboard."
# read -p "Press enter to exit" dummyVar
