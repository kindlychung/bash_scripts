#!/usr/bin/env bash

######################################################################
####  settings
######################################################################
# alt + right click, for resizing windows
gconftool-2 -s -t bool /apps/metacity/general/resize_with_right_button true
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button true

# app shortcuts
## add empty keybinding entries
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings \
    "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/']"

## browser
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "browser"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh chromium-browser"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Primary><Shift><Alt>b"
## file manager
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "fileManager"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh dolphin"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Primary><Shift><Alt>f"
## terminal
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ name "terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh gnome-terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ binding "<Primary><Shift><Alt>t"
## gmrun
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ name "runDialog"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh gmrun"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ binding "<Primary><Shift><Alt>g"

######################################################################
####  end settings
######################################################################

# ppa
sudo add-apt-repository ppa:fcitx-team/nightly
sudo add-apt-repository ppa:webupd8team/java
sudo add-apt-repository ppa:hydr0g3n/qbittorrent-stable

# install apps
sudo apt-get install \
    inkscape gimp gmrun chromium-browser thunar qbittorrent \
    virtualbox aptitude wine \
rar unrar p7zip-full \
pyside-tools qtcreator \
git bumblebee bumblebee-nvidia primus nvidia-331 python-appindicator \
 python3-pyside python3-pip \
    fcitx fcitx-table-wubi fcitx-pinyin \
    oracle-java7-installer 

pip install watchdog
