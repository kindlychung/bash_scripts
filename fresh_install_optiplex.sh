#!/usr/bin/env bash


# this comes first
sudo visudo
# ppa
# dmd
sudo wget http://master.dl.sourceforge.net/project/d-apt/files/d-apt.list -O /etc/apt/sources.list.d/d-apt.list
sudo apt-get update && sudo apt-get -y --allow-unauthenticated install --reinstall d-apt-keyring
sudo add-apt-repository ppa:fcitx-team/nightly
sudo add-apt-repository ppa:webupd8team/java
# sudo add-apt-repository ppa:hydr0g3n/qbittorrent-stable
# sudo add-apt-repository ppa:noobslab/themes
# sudo add-apt-repository ppa:webupd8team/themes
# sudo add-apt-repository ppa:noobslab/themes
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install -y \
    aptitude \
    dconf-editor \
    dolphin\
    fcitx \
    fcitx-pinyin \
    fcitx-table-wubi \
    gimp \
    git \
    gmrun \
    gnome-screensaver \
    gnome-tweak-tool \
    mpg321 \
    mplayer \
    smplayer\
    inkscape \
    ksnapshot \
    libsox-fmt-all \
    mp3splt \
    oracle-java8-installer \
    p7zip-full \
    pyside-tools \
    python3-pip \
    python3-pyside \
    python-appindicator \
    qbittorrent \
    rar \
    r-base \
    sox \
    thunar \
    unity-tweak-tool \
    unrar \
    vim-gtk \
    wine \
    xdotool\
    xsel \
    xtrlock \
    && \
sudo pip3 install watchdog pathlib
# echo 'pointer = 1 2 3 5 4 7 6 8 9 10 11 12' > ~/.Xmodmap && xmodmap ~/.Xmodmap
./dotfileSymlink.sh
source ~/.bashrc
source ~/.bash_aliases
vinstall
fc-cache -fv ~/.fonts


######################################################################
####  settings
######################################################################

# alt + right click, for resizing windows
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button true
gsettings set org.gnome.desktop.wm.preferences focus-new-windows strict
gsettings set org.gnome.desktop.wm.preferences focus-mode click
gsettings set org.gnome.desktop.wm.preferences auto-raise true
# gsettings set org.gnome.settings-daemon.peripherals.touchpad scroll-method two-finger-scrolling
# gsettings set org.gnome.settings-daemon.peripherals.touchpad natural-scroll true
# gsettings set org.gnome.desktop.input-sources xkb-options "['caps:swapescape']"
# gsettings set org.gnome.desktop.interface text-scaling-factor 1.5
# gsettings set org.gnome.desktop.wm.preferences theme 'Zukiwi'
# gsettings set org.gnome.desktop.interface gtk-theme "Zukiwi"



# app shortcuts
## add empty keybinding entries
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings \
    "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/', \
    '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/']"

## browser
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "browser"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh firefox"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Primary><Shift><Alt>b"
## file manager
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "fileManager"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh thunar"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Primary><Shift><Alt>f"
## terminal
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ name "terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh gnome-terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ binding "<Primary><Shift><Alt>t"
## gmrun
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ name "runDialog"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ command "/home/kaiyin/workspace/bash_scripts/focusRun.sh gmrun"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ binding "<Primary><Shift><Alt>g"

######################################################################
####  end settings
######################################################################


######################################################################
### softwares that arwin needs
######################################################################
sudo apt-get install igv
# seqtk
cd /tmp
git clone https://github.com/lh3/seqtk.git
cd seqtk/
make
sudo cp seqtk trimadap /usr/local/bin/

# lastz (alignment software)
git clone https://kindlychung@bitbucket.org/kindlychung/lastz.git
cd /tmp/lastz/src/
make
sudo cp lastz lastz_D /usr/local/bin/

# bowtie2
cd /tmp
wget  -O bowtie.zip http://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.2.3/bowtie2-2.2.3-linux-x86_64.zip?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fbowtie-bio%2Ffiles%2Fbowtie2%2F2.2.3%2F&ts=1413293724&use_mirror=netcologne
7z x bowtie.zip
cd bowtie2-2.2.3/
sudo cp bowtie2* /usr/local/bin

# blat
cd /tmp
wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64.v287/blat/blat
sudo cp blat /usr/local/bin/

# biopython
cd /tmp
wget http://biopython.org/DIST/biopython-1.64.zip
7z x biopython-1.64.zip
cd biopython-1.64/
python setup.py build
sudo python setup.py install

# ARC
cd /tmp
git clone --depth=1 git://github.com/ibest/ARC.git
cd ARC/
git checkout develop
sudo python setup.py install

# flash
cd /tmp
wget -O flash.tgz http://downloads.sourceforge.net/project/flashpage/FLASH-1.2.11.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fflashpage%2Ffiles%2F&ts=1413294783&use_mirror=heanet
tar zxf flash.tgz
cd FLASH-1.2.11/
make
sudo cp flash /usr/local/bin

# seqyclean
cd /tmp
git clone --depth=1 https://kindlychung@bitbucket.org/izhbannikov/seqyclean.git
cd seqyclean/
make
sudo cp bin/seqyclean /usr/local/bin/

# screen_duplicates
sudo wget -O /usr/local/bin/screen_duplicates_PE.py https://github.com/ibest/GRC_Scripts/blob/master/screen_duplicates_PE.py
sudo chmod a+x /usr/local/bin/screen_duplicates_PE.py

# novocraft
cd /tmp
wget https://bitbucket.org/kindlychung/filestore/downloads/novosortV1.03.01.Linux3.0.tar.gz
wget https://bitbucket.org/kindlychung/filestore/downloads/novocraftV3.02.07.Linux3.0.tar.gz
tar zxf novocraftV3.02.07.Linux3.0.tar.gz
tar zxf novosortV1.03.01.Linux3.0.tar.gz
sudo cp novocraft /usr/local/bin/
if [[ ":$PATH:" == *":/usr/local/bin/novocraft:"* ]]
then
    true
else
    source /etc/environment
    sudo cp /etc/environment /etc/environment.bk
    sudo chmod 666 /etc/environment
    echo PATH="/usr/local/bin/novocraft:$PATH" > /etc/environment
    sudo chmod 644 /etc/environment
fi
source /etc/environment
######################################################################
### end softwares that arwin needs
######################################################################
