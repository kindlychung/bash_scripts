#!/bin/bash

cat <<EOF
#####################################
You need to provide a dir for backup,
Please check that the dir is correct!
#####################################
As a convention, the path should end
with "backup_blah_blah".
#####################################
EOF

backup_dir=$1
cat >/tmp/exclude.rsync <<EOF
.dbus
.cache
.wine
R
opt
share*
winUsers
EOF
rsync -av --progress --delete  --exclude-from="/tmp/exclude.rsync" "$HOME" "$backup_dir"

