#!/usr/bin/env bash

shopt -s dotglob
cd
ln -sf ~/personal_config_bin_files/* .
shopt -u dotglob
